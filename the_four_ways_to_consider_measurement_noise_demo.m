% Copyright (c) 2021 Sebastian Reuschen
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
clc,clear,format compact,rng('default')
%% Define models and measurement noise 
demo_id = 2; % Defines which test scenario should be visualized.
if(demo_id==1)
    %Define measurement noise distribution
    std_meas = 0.3;
    error_pdf = makedist('Normal','mu',0,'sigma',std_meas); 
    %Define predictive PDFs of models
    all_pds  = {};
    all_pds{1} = makedist('Normal','mu',0,'sigma',0.2); 
    all_pds{2} = makedist('Normal','mu',3,'sigma',1); 
    all_pds{3} = makedist('Normal','mu',-2,'sigma',2); 
elseif(demo_id==2)
    %Define measurement noise distribution
    std_meas = 0.5;    
    error_pdf = makedist('Normal','mu',0,'sigma',std_meas); 
    %Define predictive PDFs of models
    all_pds  = {};
    all_pds{3} = makedist('Exponential','mu',2);
    all_pds{2} = makedist('Normal','mu',2,'sigma',1);
    all_pds{1} = makedist('Uniform','lower',-1.5,'upper',-0.5);
elseif(demo_id==3)
    %Define measurement noise distribution
    std_meas = 0.5;    
    error_pdf = makedist('Normal','mu',0,'sigma',std_meas); 
    %Define predictive PDFs of models
    all_pds  = {};
    all_pds{1} = makedist('Uniform','lower',-1,'upper',0);
    all_pds{2} = makedist('Uniform','lower',0.2,'upper',1.2);
    all_pds{3} = makedist('Normal','mu',2,'sigma',3);
end

%% Calculate PDFs with and without noise 
%Values that should be tested for visulaization.
true_value_all = -5:0.05:5;

%number realization of random noise
N_reali= 10000;

%dicretization for convolution. Used to calculate the PDFs with noise. 
dx = 0.01;
x_discete = -100:dx:100;
x12 = linspace(2*x_discete(1),2*x_discete(end),2*length(x_discete)-1);
error_pdf_discrete = pdf(error_pdf,x_discete);
prior_density = ones(numel(all_pds),1)*1/numel(all_pds);
for i=1:numel(all_pds)
    pdf_discrete{i} = pdf(all_pds{i},x_discete);
    pdf_with_noise_discrete{i} = conv(pdf_discrete{i},error_pdf_discrete)*dx;
end

%% Plot pdfs with and without noise
x = min(true_value_all):0.01:max(true_value_all);
for i=1:numel(all_pds)
    pdf_all_without_noise(:,i) = interp1(x_discete,pdf_discrete{i},x) ; 
    pdf_all_with_noise(:,i)    = interp1(x12,pdf_with_noise_discrete{i},x);
end
%Plot pdfs
subplot(2,3,1);
plot(x,pdf_all_without_noise);
title("PDFs without noise");xlabel("Predicted system state");ylabel("PDFs");
subplot(2,3,4);
plot(x,pdf_all_with_noise);
title("PDFs with noise");xlabel("Predicted system state");ylabel("PDFs");

%% Loop over the 4 cases
for case_i=1:4
    clear BME_data weighted_data
    if(case_i==1)
        add_noise_to_data  = 0;
        add_noise_to_model = 0;
    elseif(case_i==2)
        add_noise_to_data  = 1;
        add_noise_to_model = 0;
    elseif(case_i==3)
        add_noise_to_data  = 0;
        add_noise_to_model = 1;
    elseif(case_i==4)
        add_noise_to_data  = 1;
        add_noise_to_model = 1;
    end

    %% Calculat posterior model weight given true value
    %Define data
    for idx = 1:size(true_value_all,2)
        true_value = true_value_all(idx);
        if(add_noise_to_data)
            data = true_value + random(error_pdf,1,N_reali);
        else
            data = true_value;
        end
        %Calculate BME
        for i=1:numel(all_pds)
            if(add_noise_to_model)
                BME_data(i,:,:) = interp1(x12,pdf_with_noise_discrete{i},data(:,:));%Discretized approximation
            else
                BME_data(i,:,:) =  pdf(all_pds{i},data(:,:));
            end
        end
        %Calculate posterior model weights
        for i=1:size(BME_data,1)
            weighted_data(i,1,:) = BME_data(i,:,:)*prior_density(i);
        end
        post_weights_data = mean(weighted_data./sum(weighted_data,1),3);
        case_density(idx,:,case_i) = post_weights_data;
    end
    %Plot expexted posterior model weight
    subplot(2,3,case_i+ ceil(case_i/2));
    plot(true_value_all,case_density(:,:,case_i));
    title(["Case " + num2str(case_i)]);xlabel("True system state");ylabel("Model weights");
    
    %% Calulate MCM
    %Generate data from model M
    if(add_noise_to_data)
        for i=1:numel(all_pds)
            M_data(i,:) = random(all_pds{i},1,N_reali) + random(error_pdf,1,N_reali);
        end
    else
        for i=1:numel(all_pds)
            M_data(i,:) = random(all_pds{i},1,N_reali) ;
        end 
    end
    %Calculate BME   
    for i=1:size(M_data,1)    
       if(add_noise_to_model)
            BME(i,:,:) = interp1(x12,pdf_with_noise_discrete{i},M_data(:,:));%Discretized approximation
       else
            BME(i,:,:) =  pdf(all_pds{i},M_data(:,:));
       end
    end
    %Calculate model confusion matrix (MCM)
    for i=1:size(BME,1)
        weighted(i,:,:)     = BME(i,:,:)     *prior_density(i);
    end
    post_weights        = mean(weighted./sum(weighted,1),3);
    disp(["Case " + num2str(case_i) + ":"])
    MCM                 = post_weights
end